﻿using Microsoft.AspNet.Identity;
using Profiler.Helpers;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Profiler.Models
{
    [Table("UserPersonalInformation")]
    public class UserPersonalInfo
    {
        [Key]
        [SkipPropertyAttribute]
        public int Id { get; set; }

        [StringLength(200)]
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public virtual ApplicationUser UserObj { get; set; }

    }
}