﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Profiler.Models
{
    [Table("Competency_Rating_Mapper")]
    public class Competency_Rating
    {
        [Key]
        public int Id { get; set; }
        public virtual Competency CompetencyObj { get; set; }
        public virtual Rating RatingObj { get; set; }
        
    }
}