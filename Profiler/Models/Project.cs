﻿using Profiler.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Profiler.Models
{
    [Table("Projects")]
    public class Project
    {
        [Key]
        [SkipPropertyAttribute]
        public int Id { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public string Description { get; set; }
        [StringLength(200)]
        public string Organization { get; set; }
        [StringLength(200)]
        public string Url { get; set; }
        public virtual ApplicationUser UserId { get; set; }
        public DateTime StartDuration { get; set; }
        public DateTime EndDuration { get; set; }
    }
}