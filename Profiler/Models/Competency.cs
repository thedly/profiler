﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Profiler.Models
{
    [Table("Competencies")]
    public class Competency
    {
        [Key]
        public int Id { get; set; }
        [StringLength(255), Required]
        public string Name { get; set; }
    }
}