﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Profiler.Models
{
    [Table("Competency_Projects_Mapper")]
    public class Competency_Project
    {
        [Key]
        public int Id { get; set; }
        public virtual Competency competencyObj { get; set; }
        public virtual Project ProjectObj { get; set; }
    }
}   