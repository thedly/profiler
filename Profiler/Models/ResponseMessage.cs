﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace Profiler.Models
{
    public class ResponseMessage
    {
        public bool WasSuccessful { get; set; }
        public object Output { get; set; }
        public HttpStatusCode StatusCode { get; set; }
    }
}