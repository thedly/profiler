﻿using Profiler.EntityFramework;
using Profiler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace Profiler.Helpers
{
    public static class LoggedInUser
    {
        public static string GetCurrentUserId(IIdentity _userIdentity)
        {
            var identity = _userIdentity as ClaimsIdentity;
            return identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
        }


    }
}


