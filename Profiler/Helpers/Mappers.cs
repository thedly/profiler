﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Profiler.Helpers
{
    public static class Mappers<T>
    {
        public static T EquateObject(T obj, T ComparedObj)
        {
            IEnumerable<PropertyInfo> properties = typeof(T).GetProperties().Where(pi => pi.GetCustomAttributes(typeof(SkipPropertyAttribute), true).Length == 0).ToArray();
            Type objType = obj.GetType();
            Type ComparedObjType = ComparedObj.GetType();

            foreach (var property in properties)
            {

                var comparedValue = ComparedObjType.GetProperty(property.Name).GetValue(ComparedObj);

                if (comparedValue != null && !string.IsNullOrEmpty(comparedValue.ToString()))
                {
                    objType.GetProperty(property.Name).SetValue(obj, comparedValue);
                }
                
                
            }
            return obj;
        }
    }
}