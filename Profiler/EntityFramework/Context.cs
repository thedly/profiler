﻿using Microsoft.AspNet.Identity.EntityFramework;
using Profiler.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Profiler.EntityFramework
{
    public class Context: IdentityDbContext<ApplicationUser>
    {
        public Context(): base("DefaultConnection") { }


        public DbSet<UserPersonalInfo> UserPersonalInformation { get; set; }
        public DbSet<Competency> Competencies { get; set; }
        public DbSet<Competency_Rating> CompetencyRatingMapper { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Competency_Project> CompetencyProjectMapper { get; set; }

        public static Context Create()
        {
            return new Context();
        }
    }
}