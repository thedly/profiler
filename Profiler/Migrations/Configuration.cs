namespace Profiler.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Profiler.EntityFramework.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Profiler.EntityFramework.Context context)
        {


            context.Ratings.AddOrUpdate(
                new Models.Rating { Id = 1, Title = "Novice" },
                new Models.Rating { Id = 2, Title = "Intermediate" },
                new Models.Rating { Id = 3, Title = "Advanced" },
                new Models.Rating { Id = 4, Title = "Expert" }
            );


            context.Competencies.AddOrUpdate(
                new Models.Competency { Id = 1, Name = "ASP.NET" },
                new Models.Competency { Id = 2, Name = "SQL Server" },
                new Models.Competency { Id = 2, Name = "WPF" },
                new Models.Competency { Id = 3, Name = "WCF" },
                new Models.Competency { Id = 4, Name = "Xamarin" },
                new Models.Competency { Id = 5, Name = "Android" },
                new Models.Competency { Id = 6, Name = "Swift" }
            );


            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
