﻿using Profiler.EntityFramework;
using Profiler.Helpers;
using Profiler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Profiler.Controllers
{
    public class ProjectController : ApiController
    {
        private Context _context;

        public ProjectController()
        {
            _context = new Context();
        }

        [HttpGet]
        public IHttpActionResult GetProjects(string UsrId)
        {
            try
            {
                return Json(_context.Projects.Include("userId").Where(proj => proj.UserId.Id.Equals(UsrId)).Select(usr => new { id = usr.Id, Name = usr.Title, Description = usr.Description, Organization = usr.Organization, Url = usr.Url, userId = usr.UserId.Id, startDate = usr.StartDuration, endDate = usr.EndDuration }).ToList());
            }
            catch (Exception ex)
            {
                return Json(new ResponseMessage { Output = ex.Message, WasSuccessful = false });
            }
            
        }

        public IHttpActionResult CreateProject(Project _projectObj)
        {
            try
            {
                var currentUser = _context.Users.FirstOrDefault(usr => usr.UserName.Equals(User.Identity.Name));

                if(currentUser != null)
                {
                    _projectObj.UserId = currentUser;
                    _context.Projects.Add(_projectObj);
                    _context.SaveChanges();
                    return Json(new ResponseMessage { Output = "", WasSuccessful = true });
                }
                throw new Exception("Invalid token");
            }
            catch (Exception ex)
            {
                return Json(new ResponseMessage { Output = ex.Message, WasSuccessful = false });
            }
            
        }

        public IHttpActionResult UpdateProject([FromUri]int id, Project _projectObj)
        {
            try
            {
                var currentUser = _context.Users.FirstOrDefault(usr => usr.UserName.Equals(User.Identity.Name));

                var currentProject = _context.Projects.Find(id);
                if (currentUser != null && currentProject != null)
                {
                    if(currentProject.UserId.Equals(currentUser))
                    {
                        var mappedObj = Mappers<Project>.EquateObject(currentProject, _projectObj);

                        _context.Entry(currentProject).CurrentValues.SetValues(mappedObj);
                        _context.SaveChanges();
                        return Json(new ResponseMessage { Output = "", WasSuccessful = true });
                    }
                    throw new Exception("You are not authorized to update this project");
                }
                throw new Exception("Invalid token");
            }
            catch (Exception ex)
            {
                return Json(new ResponseMessage { Output = ex.Message, WasSuccessful = false });
            }

        }
    }
}
