﻿using Profiler.EntityFramework;
using Profiler.Helpers;
using Profiler.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Http.Results;
using System.Threading;
using System.Threading.Tasks;

namespace Profiler.Controllers
{
    public class UserInfoController : ApiController
    {

        private Context _context;

        public UserInfoController()
        {
            _context = new Context();
        }

        [HttpGet]
        public HttpResponseMessage GetUserPersonalInfo(string UsrId)
        {
            try
            {
                var res = Json(_context.UserPersonalInformation.Include("UserId").Where(usrId => usrId.UserObj.Id.Equals(UsrId)).Select(usr => new { userId = usr.UserObj.Id, Name = usr.Name, id = usr.Id, dob = usr.DateOfBirth }).ToList());
                return Request.CreateResponse(HttpStatusCode.OK, new ResponseMessage { Output = res.Content, StatusCode = HttpStatusCode.OK, WasSuccessful = true });
            }
            catch (HttpResponseException ex)
            {
                return Request.CreateResponse(ex.Response.StatusCode, new ResponseMessage { Output = ex.Response.ReasonPhrase, WasSuccessful = ex.Response.IsSuccessStatusCode });
            }
            
        }

        [HttpPost]
        [Authorize]
        public HttpResponseMessage CreateUserPersonalInfo(UserPersonalInfo _usrPersonalInfo)
        {
            try
            {

                var currentUser = _context.Users.FirstOrDefault(x => x.UserName.Equals(User.Identity.Name));

                if (currentUser != null)
                {
                    _usrPersonalInfo.UserObj = currentUser;
                    _context.UserPersonalInformation.Add(_usrPersonalInfo);
                    _context.SaveChanges();
                    Request.CreateResponse(HttpStatusCode.OK, new ResponseMessage { Output = string.Empty, WasSuccessful = true });
                }
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            catch (HttpResponseException ex)
            {
                return Request.CreateResponse(ex.Response.StatusCode, new ResponseMessage { Output = ex.Response.ReasonPhrase, WasSuccessful = ex.Response.IsSuccessStatusCode });
            }
        }
        [HttpPost]
        [Authorize]
        public HttpResponseMessage UpdateUserPersonalInfo([FromUri]int Id, UserPersonalInfo _usrPersonalInfo)
        {
            try
            {
                var usrObject = _context.UserPersonalInformation.Find(Id);
                if (usrObject != null && User.Identity.Name.Equals(usrObject.UserObj.UserName))
                {

                    var mappedObj = Mappers<UserPersonalInfo>.EquateObject(usrObject, _usrPersonalInfo);
                    _context.Entry(usrObject).CurrentValues.SetValues(mappedObj);
                    _context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, new ResponseMessage { Output = string.Empty, WasSuccessful = true });
                }
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            catch (HttpResponseException ex)
            {
                return Request.CreateResponse(ex.Response.StatusCode, new ResponseMessage { Output = ex.Response.ReasonPhrase, WasSuccessful = ex.Response.IsSuccessStatusCode });
            }

        }

        [HttpDelete]
        [Authorize]
        public HttpResponseMessage DeleteUserPersonalInfo([FromUri]int Id)
        {
            try
            {
                var usrObject = _context.UserPersonalInformation.Find(Id);

                if (User.Identity.Name.Equals(usrObject.Name))
                {
                    _context.UserPersonalInformation.Remove(usrObject);
                    _context.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, new ResponseMessage { Output = "Deleted User successfully" });
            }
            catch (HttpResponseException ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new ResponseMessage { Output = ex.Response.ReasonPhrase, WasSuccessful = ex.Response.IsSuccessStatusCode, StatusCode = ex.Response.StatusCode });
            }

        }
        
    }
}
